package com.example.alex.menuapp;
import java.math.BigDecimal;
import java.util.ArrayList;

//This is the java file that will keep track of the items a customer has on order
public class OrderBook
{
    //Volatile prevents the OrderBook instance being locally cached in a thread
    private static volatile OrderBook orderBookInstance = null;

    private ArrayList<OrderItem> orderItems;

    private OrderBook()
    {
        orderItems = new ArrayList<>();
    }

    public static OrderBook getOrderBook()
    {
        //If instance not created
        if(orderBookInstance == null)
        {
            //Create instance
            orderBookInstance = new OrderBook();
        }
        return orderBookInstance;
    }

    public int getNumItemsOnOrder()
    {
        return orderItems.size();
    }

    //TODO: Implement order total method
    public BigDecimal getOrderTotal()
    {
        return BigDecimal.ZERO;
    }
}
