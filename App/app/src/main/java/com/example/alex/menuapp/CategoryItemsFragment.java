package com.example.alex.menuapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

public class CategoryItemsFragment extends Fragment {

    private JSONArray itemsList = new JSONArray();
    private LayoutInflater inflater;
    private ListView catItemsListView;
    private TextView categoryTitleText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cat_item, container, false);
        this.inflater = inflater;

        setUpTextView(view);
        setUpListView(view);

        return view;
    }

    private void setUpTextView(View view)
    {
        this.categoryTitleText = view.findViewById(R.id.categoryTitleText);
        // Underline the text
        categoryTitleText.setPaintFlags(categoryTitleText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        //Make it bold
        categoryTitleText.setTypeface(null, Typeface.BOLD);
    }

    private void setUpListView(View view)
    {
        this.catItemsListView = view.findViewById(R.id.catItemsListView);
        this.catItemsListView.setDivider(null); //Remove lines between list items

        //Fill the view with data from the QueryAPI cache
        try
        {
            JSONArray menuCategories = QueryAPI.ALL_MENU_ITEMS.download();

            if(menuCategories!= null)
            {
                for(int i = 0; i < menuCategories.length(); ++i)
                {
                    JSONObject jsonObject = menuCategories.getJSONObject(i);

                    //If the menu category matches the one saved
                    if(jsonObject.has("cat_id") && jsonObject.getString("cat_id").equals(QueryAPI.getSavedCatId()))
                    {
                        //Set the name of the category as the title text
                        categoryTitleText.setText(jsonObject.getString("name"));

                        //Get the items from the menu category if items exist in an array format
                        if(jsonObject.has("items") && !jsonObject.isNull("items"))
                        {
                            itemsList = jsonObject.getJSONArray("items");
                        }
                        break;
                    }
                }
            }
        }
        catch(Exception e)
        {
            Helper.manageException(e);
        }

        //Load the item list into the ListView
        catItemsListView.setAdapter(new ItemListAdaptor());
    }

    //Adapter that is used to load the menu categories into the ListView when downloaded
    private class ItemListAdaptor extends BaseAdapter
    {
        //Gets the total number of menu categories
        @Override
        public int getCount()
        {
            return itemsList.length();
        }

        //Gets an menu category
        @Override
        public Object getItem(int i)
        {
            try
            {
                return itemsList.get(i);
            }
            catch (Exception e)
            {
                Helper.manageException(e);
            }
            return null;
        }


        //Get the id of a particular item on the list
        @Override
        public long getItemId(int i)
        {
            return i; // The index is the id
        }

        //Gets the view for the item list
        @Override
        public View getView(final int i, View view, ViewGroup viewGroup)
        {
            view = inflater.inflate(R.layout.fragment_cat_item_relative_view, null);

            TextView menuItemTitleText = view.findViewById(R.id.menuItemTitleText);
            TextView menuItemDescriptionText = view.findViewById(R.id.menuItemDescriptionText);
            TextView menuItemPriceText = view.findViewById(R.id.menuItemPriceText);
            ImageView menuItemImage = view.findViewById(R.id.menuItemImage);

            //Make title  bold
            menuItemTitleText.setTypeface(null, Typeface.BOLD);

            try
            {
                // Get the JSONObject containing the information for the menu items
                JSONObject menuItem = (JSONObject)getItem(i);

                // Change the title and description text
                menuItemTitleText.setText(menuItem.getString("itemName"));
                menuItemDescriptionText.setText(menuItem.getString("itemDesc"));

                // Set the text for the prices
                menuItemPriceText.setText(Helper.getPricesText(menuItem.getJSONObject("Meals").getJSONArray("meals")));

                // Load an image if it has one
                if(menuItem.has("imagePath") && !menuItem.isNull("imagePath"))
                {
                    // Get the URL and remove any backslashes
                    String imageURL = menuItem.getString("imagePath").replace("\\", "");

                    //If the image is in the cache then simply load it
                    if(ImageDownloadingAgent.imageIsInCache(imageURL))
                    {
                        menuItemImage.setImageBitmap(ImageDownloadingAgent.getImageFromCache(imageURL));
                    }
                    //Otherwise create an asynchronous task to load it
                    else
                    {
                        // Create an imageDownloadingAgent and tell it to load the image
                        new ImageDownloadingAgent(menuItemImage).execute(imageURL);
                    }
                }

                //TODO: Update price and image
            }
            catch(Exception e)
            {
                Helper.manageException(e);
            }

            return view;
        }
    }
}
