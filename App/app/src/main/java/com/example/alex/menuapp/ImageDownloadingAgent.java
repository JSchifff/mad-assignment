package com.example.alex.menuapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.HashMap;

public class ImageDownloadingAgent extends AsyncTask<String, Void, Bitmap>
{
    private static HashMap<String, Bitmap> imageCache = new HashMap<>();

    ImageView bmImage;
    String url;

    public ImageDownloadingAgent(ImageView bmImage) {
        this.bmImage = bmImage;
        url = "";
    }

    protected Bitmap doInBackground(String... urls) {
        url = urls[0];

        //If image is cached in memory then return the cached version
        if(ImageDownloadingAgent.imageIsInCache(url))
        {
            return ImageDownloadingAgent.getImageFromCache(url);
        }

        Bitmap downloadedImageData = null;
        try
        {
            InputStream in = new java.net.URL(url).openStream();
            downloadedImageData = BitmapFactory.decodeStream(in);
        }
        catch (Exception e)
        {
            Helper.manageException(e);
        }
        return downloadedImageData;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);

        ImageDownloadingAgent.addImageToCache(url, result);
    }

    public static void addImageToCache(String url, Bitmap data)
    {
        imageCache.put(url, data);
    }

    public static Bitmap getImageFromCache(String url)
    {
        return imageCache.get(url);
    }

    public static boolean imageIsInCache(String url)
    {
        return imageCache.containsKey(url);
    }
}