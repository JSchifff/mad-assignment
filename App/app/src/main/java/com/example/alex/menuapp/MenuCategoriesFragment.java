package com.example.alex.menuapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONObject;

public class MenuCategoriesFragment extends Fragment
{
    private JSONArray menuCategories;
    private LayoutInflater inflater;
    private ListView menuCategoriesListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_menu_categories, container, false);
        this.inflater = inflater;
        this.menuCategoriesListView = view.findViewById(R.id.menuCategoriesListView);
        this.menuCategoriesListView.setDivider(null); //Remove lines between list items

        //Download the menu categories
        new DownloadMenuCategories().execute();

        // Inflate the layout for this fragment
        return view;
    }

    //Just downloading file for testing
    private class DownloadMenuCategories extends AsyncTask<Object, Object, JSONArray>
    {
        @Override
        protected JSONArray doInBackground(Object... objects)
        {
            try
            {
                return QueryAPI.ALL_MENU_ITEMS.download();
            }
            catch(Exception e)
            {
                Helper.manageException(e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONArray jsonArray)
        {
            if(jsonArray != null)
            {
                Log.d("Downloaded", jsonArray.toString());
                menuCategories = jsonArray;

                //Load the menu categories into the ListView
                menuCategoriesListView.setAdapter(new MenuCategoryAdaptor());
            }
        }
    }

    //Adapter that is used to load the menu categories into the ListView when downloaded
    private class MenuCategoryAdaptor extends BaseAdapter
    {
        //Gets the total number of menu categories
        @Override
        public int getCount()
        {
            return menuCategories.length();
        }

        //Gets an menu category
        @Override
        public Object getItem(int i)
        {
            try
            {
                return menuCategories.get(i);
            }
            catch (Exception e)
            {
                Helper.manageException(e);
            }
            return null;
        }


        //Get the id of a particular item on the list
        @Override
        public long getItemId(int i)
        {
            try
            {
                return ((JSONObject)getItem(i)).getLong("cat_id");
            }
            catch(Exception e)
            {
                Helper.manageException(e);
            }
            return -1;
        }

        //Gets the view for a menu category
        @Override
        public View getView(final int i, View view, ViewGroup viewGroup)
        {
            view = inflater.inflate(R.layout.menu_category_relative_view, null);
            Button menuCategoryButton = view.findViewById(R.id.menuCategoryButton);

            try
            {
                menuCategoryButton.setText(((JSONObject)getItem(i)).getString("name"));
            }
            catch(Exception e)
            {
                Helper.manageException(e);
            }

            menuCategoryButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent myIntent = new Intent(view.getContext(), CategoryItems.class);
                    try
                    {
                        //Save
                        QueryAPI.saveCatID(((JSONObject)getItem(i)).getString("cat_id"));
                    }
                    catch(Exception e)
                    {
                        Helper.manageException(e);
                    }
                    view.getContext().startActivity(myIntent);
                }
            });

            return view;
        }
    }

}
