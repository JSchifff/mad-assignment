package com.example.alex.menuapp;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

public class ItemsOnOrderBarFragment extends Fragment
{
    TextView itemsOnOrderText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_items_on_order_bar, container, false);

        itemsOnOrderText = view.findViewById(R.id.itemsOnOrderText);

        updateText();

        return view;
    }

    public void updateText()
    {
        int numItems = OrderBook.getOrderBook().getNumItemsOnOrder();
        String text = String.format(Locale.US, "%d item%s on order.", numItems, ((numItems == 1)?"":"s"));
        itemsOnOrderText.setText(text);
    }
}
