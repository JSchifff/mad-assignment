package com.example.alex.menuapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

public class Helper
{
    public static void manageException(Exception e)
    {
        Log.d("ERROR","Exception", e);
    }

    public static String getPricesText(JSONArray meals)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < meals.length(); ++i)
        {
            if(i > 0)
            {
                stringBuilder.append(", "); // Add a comma if the first price has already been added
            }

            try
            {
                JSONObject mealObject = meals.getJSONObject(i);

                //If mealsize and price information exists
                if(mealObject.has("mealsize") && mealObject.has("price"))
                {
                    stringBuilder.append(mealObject.getString("mealsize")); // Add the meal size
                    stringBuilder.append(String.format(Locale.US, ": $%.2f", mealObject.getDouble("price"))); //Add the price
                }
                else
                {
                    //Remove comma and space that was added as the object does not contain the required information
                    if(i > 0)
                    {
                        stringBuilder = new StringBuilder(stringBuilder.toString().substring(0, stringBuilder.length() - 2));
                    }
                }
            }
            catch(Exception e)
            {
                Helper.manageException(e);
            }
        }
        return stringBuilder.toString();
    }
}
