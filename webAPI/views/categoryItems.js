<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script>
        $(document).ready(function () {
            var cat_id = window.location.href.split("/").pop();

            function getItems(cat_id) {
                $.get("https://gentle-atoll-23251.herokuapp.com/api/menuItems/" + cat_id, function(data, status) {
                    console.log(data);
                    $("#title").text(data[0].name);

                    for(res of data) {
                        $("#category").append("<div class='item'>" + res.itemname + ": " + res.itemdesc + "</div>")
                    }
                
                });
            };

            $("#submit").click(function() {
                //console.log("khrg");
                var name = $("#nameField").val();
                console.log(name);
                if(name.length >= 1) {
                    $.post("https://gentle-atoll-23251.herokuapp.com/api/menuCategories",
                        "name=" + name
                    ).then(getCats());
                }
            });

            console.log(cat_id);
            getItems(cat_id);
            
        });

        
        </script>
    </head>
    <body>
        <div>
            Name: <input type="text" id="name" /><br />
            Description: <input type="text" id="desc" /><br />
            Image: <input type="file" id="img" /><br />
            Nut Free: <input type="checkbox" id="nutFree" /><br />
            Gluten Free: <input type="checkbox" id="glutenFree" /><br />
            Lactose Free: <input type="checkbox" id="lactoseFree" /><br />
            Veg friendly?: No-><input type="radio" name="vegFriendly" value="no"/>
            Vegetarian-><input type="radio" name="vegFriendly" value="vegetarian"/>
            Vegan-><input type="radio" name="vegFriendly" value="vegan" /><br />
            Available: <input type="checkbox" id="available" /><br />
            <input type="button" id="submit" value="Add Item!" />

        </div>
        <hr />
        <div id="category">
            <h1 id="title"></h1>
        </div>
    </body>
</html>