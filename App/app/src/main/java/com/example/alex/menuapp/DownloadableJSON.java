package com.example.alex.menuapp;

import org.json.JSONArray;

import java.net.URL;

public class DownloadableJSON
{

    //Volatile keyword prevents local thread caching of variables
    private volatile String endpoint;
    private volatile String path; //The api address that points to the resource (eg. menuCategories would download from "https://gentle-atoll-23251.herokuapp.com/api/menuCategories)
    private volatile JSONArray jsonFile;

    public DownloadableJSON(String endpoint, String downloadPath)
    {
        this.endpoint = endpoint;
        this.path = downloadPath;
        jsonFile = null;

    }

    //TODO: Cache downloaded data locally as a file. If the dateLastModified variable is too old then redownload (serve cached file is download fails). For caching, menuCategories would be stored in cache/menuCategories.json
    public JSONArray download() throws Exception
    {
        //If cache of download does not exists
        if(jsonFile == null)
        {
            jsonFile = QueryAPI.downloadFile(new URL(endpoint + path));
        }
        return jsonFile;
    }

    public JSONArray getJsonFile()
    {
        return jsonFile;
    }

    private void setJsonFile(JSONArray jsonArray)
    {
        jsonFile = jsonArray;
    }
}


