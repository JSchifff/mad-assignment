package com.example.alex.menuapp;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.Locale;

public class TopMenuBarFragment extends Fragment
{
    LinearLayout menuButton;
    TextView totalText;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top_menu_bar, container, false);

        menuButton = view.findViewById(R.id.menuButton);
        totalText = view.findViewById(R.id.totalText);

        updateTotalText();

        //Set up the onclick listener for the menuButton
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                onMenuButtonClick();
            }
        });
        this.view = view;
        return view;
    }

    //TODO: implement method that switches view to the top menu
    private void onMenuButtonClick()
    {
        //Go to the top menu when the menu button is pressed
        Intent myIntent = new Intent(view.getContext(), MainActivity.class);
        view.getContext().startActivity(myIntent);
    }

    private void updateTotalText()
    {
        //Get the order total from the menu book
        BigDecimal orderTotal = OrderBook.getOrderBook().getOrderTotal();

        //Form the string with with the price having two decimal places
        String orderTotalString = String.format(Locale.US, "Total: $%.2f", orderTotal.floatValue());

        //Replace the text in the TextView
        totalText.setText(orderTotalString);
    }


}
