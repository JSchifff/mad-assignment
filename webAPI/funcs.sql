CREATE TABLE menu_cats (
    cat_id     SERIAL PRIMARY KEY,
    name    text NOT NULL
);

CREATE TABLE menu_item (
    item_id SERIAL PRIMARY KEY,
    imagePath TEXT,
    itemName TEXT NOT NULL,
    itemDesc TEXT NOT NULL,
    glutenFree BOOLEAN,
    nutFree BOOLEAN,
    lactoseFree BOOLEAN,
    vegFriendly TEXT,
    available BOOLEAN NOT NULL,
    cat_id INTEGER NOT NULL,
    FOREIGN KEY(cat_id) REFERENCES menu_cats(cat_id)
);

CREATE TABLE menu_meal (
    meal_id SERIAL PRIMARY KEY,
    price REAL NOT NULL,
    mealSize TEXT NOT NULL,
    item_id INTEGER NOT NULL,
    FOREIGN KEY(item_id) REFERENCES menu_item(item_id)
);

CREATE TABLE orders (
    order_id SERIAL PRIMARY KEY,
    tableNumber TEXT,
    orderTime TIMESTAMP NOT NULL,
    orderStatus TEXT NOT NULL
);

CREATE TABLE order_meals (
    order_id INTEGER NOT NULL,
    meal_id INTEGER NOT NULL,
    orderNotes TEXT,
    quantity INTEGER NOT NULL,
    PRIMARY KEY(order_id, meal_id),
    FOREIGN KEY(order_id) REFERENCES orders(order_id),
    FOREIGN KEY(meal_id) REFERENCES menu_meal(meal_id)
);


SELECT menu_cats.*, json_agg(menu_item) AS "items"
      FROM menu_cats
      LEFT JOIN menu_item USING (cat_id)
      LEFT JOIN menu_meal USING (item_id)
      GROUP BY menu_cats.cat_id, menu_item.item_id;

SELECT 
    json_build_object(
      'CatName', mc.name, 'items', mi 
    ) cats
  from menu_cats mc
  LEFT JOIN (
    select cat_id,
    json_agg(
      json_build_object(
        'imagePath', mi.imagePath,
        'itemName', mi.itemName,
        'itemDesc', mi.itemDesc,
        'glutenFree', mi.glutenFree,
        'nutFree', mi.nutFree,
        'lactoseFree', mi.lactoseFree,
        'vegFriendly', mi.vegFriendly,
        'available', mi.available,
        'Meals', mm
      )
    ) items
    FROM menu_item mi

    LEFT JOIN (
      SELECT item_id,
      json_agg(mm.*) meals
      FROM menu_meal mm
      GROUP BY 1
    )
    mm ON mi.item_id = mm.item_id
    GROUP BY cat_id
  ) mi ON mc.cat_id = mi.cat_id;