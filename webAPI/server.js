var express = require("express");
var bodyParser = require("body-parser");
const { Client } = require('pg');
const { parse } = require('querystring');
var SqlString = require('sqlstring');

var app = express();
app.use(bodyParser.json());
app.engine('js', require('ejs').renderFile);
//app.use(express.static(__dirname + '../public'));

const client = new Client({
  connectionString: process.env.DATABASE_URL,
  ssl: true,
});

const tables = {
  "Categories": "menu_cat",
  "Items": "menu_item",
  "Meals": "menu_meal",
  "Orders": "orders",
  "OrderMeals": "order_meals"
};

/*
..######..########....###....########..########.....######..########.########..##.....##.########.########.
.##....##....##......##.##...##.....##....##.......##....##.##.......##.....##.##.....##.##.......##.....##
.##..........##.....##...##..##.....##....##.......##.......##.......##.....##.##.....##.##.......##.....##
..######.....##....##.....##.########.....##........######..######...########..##.....##.######...########.
.......##....##....#########.##...##......##.............##.##.......##...##....##...##..##.......##...##..
.##....##....##....##.....##.##....##.....##.......##....##.##.......##....##....##.##...##.......##....##.
..######.....##....##.....##.##.....##....##........######..########.##.....##....###....########.##.....##
*/



var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    client.connect();
      
    console.log("App now running on " + port);
});



/*
..######...########.########..######.
.##....##..##..........##....##....##
.##........##..........##....##......
.##...####.######......##.....######.
.##....##..##..........##..........##
.##....##..##..........##....##....##
..######...########....##.....######.
*/
app.get("/api/menuCategories", function(req, res) {
  client.query(`SELECT * FROM menu_cats;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/menuItems", function(req, res) {
  client.query(`SELECT * FROM menu_item;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/menuItems/:catID", function(req, res) {
  console.log(req.params);
  client.query(`SELECT * FROM menu_cats LEFT JOIN menu_item USING(cat_id) WHERE cat_id = ` + SqlString.escape(req.params.catID) + `;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.setHeader('Access-Control-Allow-Origin', '*');

    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/menuMeals", function(req, res) {
  client.query(`SELECT * FROM menu_meal;`, (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.status(200).json(queryRes.rows);
  });
});

app.get("/api/allMenuItems", function(req, res) {
  client.query(`
  SELECT 
    mc.cat_id,
    name,
    items
  from menu_cats mc
  LEFT JOIN (
    select cat_id,
    json_agg(
      json_build_object(
        'imagePath', mi.imagePath,
        'itemName', mi.itemName,
        'itemDesc', mi.itemDesc,
        'glutenFree', mi.glutenFree,
        'nutFree', mi.nutFree,
        'lactoseFree', mi.lactoseFree,
        'vegFriendly', mi.vegFriendly,
        'available', mi.available,
        'Meals', mm
      )
    ) items
    FROM menu_item mi

    LEFT JOIN (
      SELECT item_id,
      json_agg(mm.*) meals
      FROM menu_meal mm
      GROUP BY 1
    )
    mm ON mi.item_id = mm.item_id
    GROUP BY cat_id
  ) mi ON mc.cat_id = mi.cat_id;
  `, (err, queryRes) => {
  // client.query('SELECT * FROM menu_cats LEFT JOIN menu_item USING(cat_id);', (err, queryRes) => {
    if (err) {throw err};
    //console.log(queryRes);
    res.status(200).json(queryRes.rows);
  });
});


/*
.########...#######...######..########..######.
.##.....##.##.....##.##....##....##....##....##
.##.....##.##.....##.##..........##....##......
.########..##.....##..######.....##.....######.
.##........##.....##.......##....##..........##
.##........##.....##.##....##....##....##....##
.##.........#######...######.....##.....######.
*/
//Function that pulls needed attributes from request body, passes the relevant data to the callback
//The callback inserts the data into the database
function getRequestData(req, callback) {
  var data;
  let body = '';
    req.on('data', chunk => {
        body += chunk.toString(); // convert Buffer to string
    });
    req.on('end', () => {
        data = parse(body);
        callback(data);
    }); 
}


app.post("/api/menuItems", function(req, res) {

  getRequestData(req, postMenuCat);
        
  function postMenuCat(data) {
    var requiredProperties = [
      "name",
      "desc",
      "available",
      "cat_id"
    ];
    if(requiredProperties.every(prop => prop in data)) {
      // console.log("trying SQL:   " + data.name);

      // var query = 'INSERT INTO menu_cats(name) VALUES(\'' + SqlString.escape(body.name) + '\');';
      console.log(data.name);

      //client.query('INSERT INTO menu_cats(name) VALUES(' + SqlString.escape(data.name) + ');');
      res.json('complete: true');
    }
  }
});

app.post("/api/menuCategories", function(req, res) {
  getRequestData(req, postMenuItem);
        
  function postMenuItem(data) {
    if('name' in data) {
      // console.log("trying SQL:   " + data.name);

      // var query = 'INSERT INTO menu_cats(name) VALUES(\'' + SqlString.escape(body.name) + '\');';
      console.log("Adding " + SqlString.escape(data.name) + " to MenuCategories");

      client.query('INSERT INTO menu_cats(name) VALUES(' + SqlString.escape(data.name) + ');');
      res.json('complete: true');
    }
  }
});
/*
.##.....##.####.########.##......##..######.
.##.....##..##..##.......##..##..##.##....##
.##.....##..##..##.......##..##..##.##......
.##.....##..##..######...##..##..##..######.
..##...##...##..##.......##..##..##.......##
...##.##....##..##.......##..##..##.##....##
....###....####.########..###..###...######.
*/
  //Index Page
  app.get("/", function (req, res) {
    
    res.status(200).render("index.js", {root: __dirname});
    
  });

  app.get("/categories", function (req, res) {
    
    res.status(200).render("categories.js", {root: __dirname});
    
  });

  app.get("/categories/:catID", function (req, res) {
    
    res.status(200).render("categoryItems.js", {root: __dirname});
    
  });
