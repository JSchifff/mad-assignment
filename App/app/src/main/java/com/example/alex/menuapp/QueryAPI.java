package com.example.alex.menuapp;
import org.json.JSONArray;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class QueryAPI
{
    private static final String ENDPOINT = "https://gentle-atoll-23251.herokuapp.com/api/";

    //Possible API queries
    //public static DownloadableJSON MENU_CATEGORIES = new DownloadableJSON(ENDPOINT, "menuCategories");
    //public static DownloadableJSON MENU_ITEMS = new DownloadableJSON(ENDPOINT, "menuItems");
    //public static DownloadableJSON MENU_MEALS = new DownloadableJSON(ENDPOINT, "menuMeals");
    public static DownloadableJSON ALL_MENU_ITEMS = new DownloadableJSON(ENDPOINT, "allMenuItems");

    // Saves the category ID last selected
    private static String savedCatID = "";

    //Download a JSON file from a URL
    public static JSONArray downloadFile(URL url) throws Exception
    {
        HttpURLConnection connection;
        connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(false);
        connection.setDoInput(true);
        connection.setUseCaches(false);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        //Handle HTTP response code
        int status = connection.getResponseCode();
        //IF not 200 (OK)
        if (status != 200)
        {
            throw new IOException("Failed to retrieve JSON data from " + url.toString());
        }
        //If connection is established successfully
        else
        {
            //Create BufferedReader object to read from
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            //Create StringBuilder to append to
            StringBuilder textFromStream = new StringBuilder();
            int nextChar = in.read(); //Read first character from stream

            //Add text from stream to StringBuilder until EOF (-1) is encountered
            while (nextChar != -1)
            {
                textFromStream.append((char) nextChar);
                nextChar = in.read();
            }

            //Create JSONArray and return
            return new JSONArray(textFromStream.toString());
        }
    }

    public static void saveCatID(String text)
    {
        savedCatID = text;
    }

    public static String getSavedCatId()
    {
        return savedCatID;
    }
}
